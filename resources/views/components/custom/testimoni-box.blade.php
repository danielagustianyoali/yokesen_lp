<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-5">
                <img src="{{$img}}" alt="" class="img-testi">
            </div>
            <div class="col-7">
                <p class="testi-detail">{{$details}}</p>

                <h6 class="testi-name">{{$name}}</h6>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-3 col-md-4 col-sm-6 ">
    <div class="d-flex justify-content-center mb-5">
        <img src="{{ $asset }}" class="img-symbol">
    </div>

    <div class="layanan card">
        <div class="card-body">
            <h5 class="title text-center mb-5">
                {{ $title }}
            </h5>
            <ul>
                {!! $detail !!}
            </ul>
        </div>
    </div>


</div>

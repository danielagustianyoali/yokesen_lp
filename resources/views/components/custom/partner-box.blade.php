<div class="col-lg-3 col-md-4 col-6 my-auto">
    <div class="div-partner mb-6">
        <img src="{{$url}}" alt="" class="img-partner">
        <div class="position-absolute info">
            <div class="d-flex justify-content-center align-items-center h-100">
                <h5 style="color: white" class="text-center">
                    {{$name}}
                </h5>
            </div>
            
        </div>
    </div>

</div>
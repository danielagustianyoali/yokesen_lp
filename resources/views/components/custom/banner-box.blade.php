<div class="container " style="height: 370px">

    <div class="card h-100">
        <div class="card-body">
            <div class="d-flex align-items-center h-100">
                <div class="row">
                    <div class="col-md-6 order-md-1 order-2 my-auto">
                        <h2 class="title-banner">
                            
                            {{$title}}
                        </h2>
                        <p>
                            {{$detail}}
                           </p>
                    </div>
                    <div class="col-md-6 order-md-2 order-1 ">
                        <img src="{{$asset}}" alt=""
                            class="banner-img">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!--begin::Entry-->
@extends('master')

@section('cssinline')
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('asset/css/custom.css') }}" type="text/css">
@endsection


@section('content')
    <div class="d-flex flex-column-fluid flex-column position-relative" id="content-data">
        <section class="banner-hero mt-8">
            <h1 class="title-part text-center">Keluarga Besar Yokesen Meliputi:</h1>
            <div class="inside-padding"></div>
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">

                        @component('components.custom.banner-box')
                            @slot('title')
                                gudangin
                            @endslot
                            @slot('detail')
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s, when an unknown printer took a galley of type and
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('asset')
                                https://dashboard.gudangin.id/images/gudangin.png
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">@component('components.custom.banner-box')
                            @slot('title')
                                Kopi Grontol
                            @endslot
                            @slot('detail')
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s, when an unknown printer took a galley of type and
                                scrambled it to
                            @endslot
                            @slot('asset')
                                {{ asset('images/logo-kopi-grontol.png') }}
                            @endslot
                        @endcomponent</div>
                    <div class="swiper-slide">@component('components.custom.banner-box')
                            @slot('title')
                                Kagumi
                            @endslot
                            @slot('detail')
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s, when an unknown printer took a galley of type and
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('asset')
                                {{ asset('images/logo-kagumi.png') }}
                            @endslot
                        @endcomponent
                    </div>

                    <div class="swiper-slide">@component('components.custom.banner-box')
                            @slot('title')
                                Warisan
                            @endslot
                            @slot('detail')
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the industry's standard dummy text ever
                                since the 1500s, when an unknown printer took a galley of type and
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('asset')
                                {{ asset('images/logo-warisan.png') }}
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">@component('components.custom.banner-box')
                        @slot('title')
                            Resonansi
                        @endslot
                        @slot('detail')
                            Lorem Ipsum is simply dummy text of the printing and typesetting
                            industry. Lorem Ipsum has been the industry's standard dummy text ever
                            since the 1500s.
                        @endslot
                        @slot('asset')
                            {{ asset('images/logo-resonansi.png') }}
                        @endslot
                    @endcomponent
                </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </section>
        <!--begin::Container-->
        <div class="divider-landing"></div>
        <section class="section-1">
            <div class="container">
                <h2 class="title-part text-center mb-7">
                    Mengapa Yokesen?
                </h2>
                <div class="card why-card">
                    <div class="card-body">
                        <div class="inside-padding"></div>
                        <h4 class="detail-part text-center">
                            Karena kami ada untuk membantu Anda agar pelanggan Anda memahami
                            lebih baik tentang merek atau produk Anda, membantu Anda mengatasi
                            hambatan teknologi dan memanfaatkan peluang.
                        </h4>
                        <div class="inside-padding"></div>
                    </div>
                </div>
                <div class="d-flex justify-content-around mt-7">
                    <button class="btn btn__prim">
                        Gabung Sekarang
                    </button>
                    <a href="" class="btn btn_wa">
                        <div class="d-flex align-items-center">
                            <i class="fab fa-whatsapp" style="color: white; font-size:16px; margin-right: 8px"></i>
                            Daftar Lewat WA
                        </div>

                    </a>
                </div>
            </div>
        </section>
        <div class="divider-landing"></div>
        <section class="section-2">
            <div class="inside-padding"></div>
            <h2 class="title-part text-center"> Layanan Apa Saja yang Disediakan Yokesen?</h2>
            <div class="container">
                <div class="row justify-content-center mt-10">
                    @component('components.custom.layanan-box')
                        @slot('asset')
                            {{ asset('images/technology.png') }}
                        @endslot
                        @slot('title')
                            Technology
                        @endslot
                        @slot('detail')
                            <li>
                                Website architecture, design & prototyping
                            </li>
                            <li>
                                Conversion focused website and landing page, tested for optimum results

                            </li>
                            <li>
                                User Experience, Front-End Development, User Interface Design

                            </li>
                            <li>
                                Mobile Application development

                            </li>
                            <li>
                                Software as a Service (SaaS)
                            </li>
                            <li>Software as a Business (SaaB)</li>
                        @endslot
                    @endcomponent

                    @component('components.custom.layanan-box')
                        @slot('asset')
                            {{ asset('images/digital-strategy.png') }}
                        @endslot
                        @slot('title')
                            Digital Strategy
                        @endslot
                        @slot('detail')
                            <li>
                                Digital Branding, Digital Media and Gamification Design & development
                            </li>
                            <li>
                                Audience Insight, Targeting & Discovery

                            </li>
                            <li>
                                Media Planning & Buying

                            </li>
                            <li>
                                Search Engine Optimization (SEO)

                            </li>
                            <li>
                                Conversion Rate Optimization
                            </li>
                            <li>KOL/influencer marketing</li>
                            <li>Lead Generation and Data collection</li>
                            <li>Reporting and Dashboards</li>
                        @endslot
                    @endcomponent

                    @component('components.custom.layanan-box')
                        @slot('asset')
                            {{ asset('images/consultant.png') }}
                        @endslot
                        @slot('title')
                            Technology Consulting
                        @endslot
                        @slot('detail')
                            <li>
                                Data-driven Strategy, from eCommerce to complex software, platforms and mobile apps essential for
                                business growth
                            </li>
                            <li>
                                Conversion focused Optimization
                            </li>
                            <li>
                                Performance management

                            </li>
                            <li>
                                Project management

                            </li>
                            <li>
                                Balanced Scorecard

                            </li>

                        @endslot
                    @endcomponent
                    @component('components.custom.layanan-box')
                        @slot('asset')
                            {{ asset('images/marketplace.png') }}
                        @endslot
                        @slot('title')
                            Marketplace Optimization
                        @endslot
                        @slot('detail')
                            <li>
                                Full Marketplace Management

                            </li>
                            <li>
                                Digital store, product listing, and digital shelving

                            </li>
                            <li>
                                Marketplace creative campaign & promotion

                            </li>
                            <li>
                                Organic & paid marketplace Optimization

                            </li>
                            <li>
                                Sales & Conversion Rate Optimization
                            </li>
                            <li>Fulfilment & logistic distribution
                            </li>
                            <li>Hyperlocal - Consolidated E-commerce Dashboard (CED)</li>
                        @endslot
                    @endcomponent
                    @component('components.custom.layanan-box')
                        @slot('asset')
                            {{ asset('images/multimedia.png') }}
                        @endslot
                        @slot('title')
                            Multimedia and Digital Creative
                        @endslot
                        @slot('detail')
                            <li>
                                Full Funnel Digital Media Management

                            </li>
                            <li>
                                Media strategy & Consumer journey

                            </li>
                            <li>
                                Media strategy & Consumer journey

                            </li>
                            <li>
                                3D Animation, Motion Video, Product Photoshoot, Copywriting

                            </li>
                            <li>
                                Virtual events, live streaming and webinars

                            </li>
                            <li>TVC production
                            </li>
                            <li>Interactive Website</li>
                        @endslot
                    @endcomponent
                </div>
            </div>

        </section>
        <div class="divider-landing"></div>
        <section class="section-3">
            <h2 class="title-part text-center">
                Brand Apa Saja Yang Telah Bekerja Sama Dengan Yokesen?

            </h2>
            <div class="container">
                <div class="row mt-7 justify-content-center">
                    @component('components.custom.partner-box')
                        @slot('url')
                            https://www.larutanpenyegar.com/po-content/themes/cap-badak/images/logo.jpg
                        @endslot
                        @slot('name')
                            PT. Sinde Budi Sentosa
                        @endslot
                    @endcomponent
                    @component('components.custom.partner-box')
                        @slot('url')
                            https://images.tokopedia.net/img/cache/215-square/shops-1/2019/5/20/5275068/5275068_519a9a62-5085-42b0-acf8-c08dfe1c3933.jpg
                        @endslot
                        @slot('name')
                            Bardi
                        @endslot
                    @endcomponent
                    @component('components.custom.partner-box')
                        @slot('url')
                            https://www.bayer.com/themes/custom/bayer_cpa/logo.svg
                        @endslot
                        @slot('name')
                            Bayer Indonesia
                        @endslot
                    @endcomponent
                    @component('components.custom.partner-box')
                        @slot('url')
                            https://images.tokopedia.net/img/cache/215-square/GAnVPX/2021/1/6/e8d7acee-d872-4979-958b-938896d1d0c3.jpg
                        @endslot
                        @slot('name')
                            Delicute
                        @endslot
                    @endcomponent
                    @component('components.custom.partner-box')
                        @slot('url')
                            https://seekvectorlogo.net/wp-content/uploads/2019/04/etoro-vector-logo.png
                        @endslot
                        @slot('name')
                            eToro
                        @endslot
                    @endcomponent

                </div>
            </div>

        </section>
        <div class="divider-landing"></div>
        <section class="section-4">
            {{-- Testimoni --}}
            <h2 class="title-part text-center mb-7">Testimoni dari Customer</h2>
            <div class="swiper swiperTesti">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        @component('components.custom.testimoni-box')
                            @slot('img')
                                https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                            @endslot
                            @slot('details')
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('name')
                                John Doe
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">
                        @component('components.custom.testimoni-box')
                            @slot('img')
                                https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                            @endslot
                            @slot('details')
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('name')
                                John Doe
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">
                        @component('components.custom.testimoni-box')
                            @slot('img')
                                https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                            @endslot
                            @slot('details')
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('name')
                                John Doe
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">
                        @component('components.custom.testimoni-box')
                            @slot('img')
                                https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                            @endslot
                            @slot('details')
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('name')
                                John Doe
                            @endslot
                        @endcomponent
                    </div>
                    <div class="swiper-slide">
                        @component('components.custom.testimoni-box')
                            @slot('img')
                                https://dm0qx8t0i9gc9.cloudfront.net/watermarks/image/rDtN98Qoishumwih/empty-person-vector-concept_f1QdAxPO_SB_PM.jpg
                            @endslot
                            @slot('details')
                                scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining
                                essentially unchanged
                            @endslot
                            @slot('name')
                                John Doe
                            @endslot
                        @endcomponent
                    </div>
                </div>
            </div>
        </section>
        <div class="divider-landing"></div>
        <section class="section-form-1">
            <div class="container">
                <h2 class="title-part text-center">
                    Berminat Untuk Diskusi Dengan Account Executive Kami?
                </h2>
                <div class="row mt-8">
                    <div class="col-md-6">
                        <img class="img-calendar" src="{{ asset('images/calendar.png') }}" alt="">
                    </div>
                    <div class="col-md-6">
                        <div class="card h-100">
                            <div class="card-body">
                                <form action="" class="form-now" id="form-meet">
                                    <h4 class=" mb-6 text-center form-title" sty>Meet with Our Account Executive</h4>
                                    <input type="text" class="form-control" name="name" placeholder="Name*" required>
                                    <div class="error-text" id="error_name"></div>
                                    <input type="text" class="form-control" name="email" placeholder="Email*" required>
                                    <div class="error-text" id="error_email"></div>
                                    <input type="text" class="form-control" name="whatsapp" placeholder="Mobile Number*"
                                        required>
                                    <div class="error-text" id="error_whatsapp"></div>
                                    <input type="text" class="form-control" name="company" placeholder="Company Name*"
                                        required>
                                    <div class="error-text" id="error_company"></div>
                                    <div class="d-flex justify-content-center align-items-center mt-9">
                                        <button class="btn btn__prim">
                                            Book Now
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="divider-landing"></div>
        <section class="section-form-2">
            <div class="container">
                <h2 class="title-part text-center">
                    Tertarik Untuk Tumbuh & Terus Berkembang Bersama Yokesen?
                </h2>
                <div class="row mt-8">
                    <div class="col-md-6">
                        <div class="card h-100">
                            <div class="card-body">
                                <form action="" class="form-now" id="form-contact-us">
                                    <h4 class=" mb-6 text-center form-title" sty>Contact Us</h4>
                                    <input type="text" class="form-control" name="contact_name" placeholder="Name*"
                                        required>
                                    <div class="error-text" id="error_contact_name"></div>
                                    <input type="text" class="form-control" name="contact_email" placeholder="Email*"
                                        required>
                                    <div class="error-text" id="error_contact_email"></div>
                                    <input type="text" class="form-control" name="contact_whatsapp"
                                        placeholder="Mobile Number*" required>
                                    <div class="error-text" id="error_contact_whatsapp"></div>
                                    <input type="text" class="form-control" name="contact_company"
                                        placeholder="Company Name*" required>
                                    <div class="error-text" id="error_contact_company"></div>
                                    <textarea name="contact_message" rows="4" cols="50" placeholder="Message*" required
                                        class="form-control"></textarea>
                                    <div class="error-text" id="error_contact_message"></div>

                                    <div class="d-flex justify-content-center align-items-center mt-9">
                                        <button class="btn btn__prim">
                                            Book Now
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 my-auto">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.191023410788!2d106.64924071418785!3d-6.238535395484163!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f9ceda7c6ab5%3A0xce2a00aeec086f67!2sYOKESEN!5e0!3m2!1sen!2sid!4v1642496107751!5m2!1sen!2sid"
                            class="map-gugel" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        <div class="inside-padding"></div>
                        <h3 class="title-part text-center mb-5">Follow Social Media Kami</h3>
                        <div class="d-flex justify-content-center align-items-center">
                            <a href="" class="icon-data">
                                <img src="https://seeklogo.com/images/W/whatsapp-icon-logo-BDC0A8063B-seeklogo.com.png"
                                    alt="">
                            </a>
                            <a href="" class="icon-data">
                                <img src="https://seeklogo.com/images/F/facebook-logo-966BBFBC34-seeklogo.com.png" alt="">
                            </a>
                            <a href="" class="icon-data">
                                <img src="https://seeklogo.com/images/Y/youtube-icon-logo-521820CDD7-seeklogo.com.png"
                                    alt="">
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!--end::Container-->
        <div class="divider-landing"></div>
    </div>
@endsection


@section('jsPage')
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        var swiper = new Swiper(".mySwiper", {
            cssMode: true,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            pagination: {
                el: ".swiper-pagination",
            },
            mousewheel: true,
            keyboard: true,
        });
    </script>
    <script>
        // Valdiation
        $(function() {
            $('#form-meet').on('submit', function(e) {
                let errorName = false;
                let errorEmail = false;
                let errorWhatsapp = false;
                let errorCompany = false;

                let dataName = $('input[name="name"]').val();
                let dataEmail = $('input[name="email"]').val();
                let dataWhatsapp = $('input[name="whatsapp"]').val();
                let dataCompany = $('input[name="company"]').val();


                if (dataName.length < 3) {
                    errorName = true;
                    $("#error_name").text('Nama Tidak Boleh Kosong');
                } else {
                    errorName = false;
                    $("#error_name").text('');
                }
                if (dataEmail.length < 8) {
                    errorEmail = true;
                    $("#error_email").text('Email Tidak Boleh Kosong');
                } else {
                    errorEmail = false;
                    $("#error_email").text('');
                }
                if (dataWhatsapp.length < 9) {
                    errorWhatsapp = true;
                    $("#error_whatsapp").text('Nomor Handphone Tidak Boleh Kosong');
                } else {
                    errorWhatsapp = false;
                    $("#error_whatsapp").text('');
                }
                if (dataCompany.length < 9) {
                    errorCompany = true;
                    $("#error_company").text('Nama Perusahaan Tidak Boleh Kosong');
                } else {
                    errorCompany = false;
                    $("#error_company").text('');
                }

                if (errorCompany || errorEmail || errorName || errorWhatsapp) {
                    e.preventDefault();
                }
            })
            $('#form-contact-us').on('submit', function(e) {
                let errorName = false;
                let errorEmail = false;
                let errorWhatsapp = false;
                let errorCompany = false;
                let errorMessage = false;

                let dataName = $('input[name="contact_name"]').val();
                let dataEmail = $('input[name="contact_email"]').val();
                let dataWhatsapp = $('input[name="contact_whatsapp"]').val();
                let dataCompany = $('input[name="contact_company"]').val();
                let dataCompany = $('input[name="contact_message"]').val();

                if (dataName.length < 3) {
                    errorName = true;
                    $("#error_contact_name").text('Nama Tidak Boleh Kosong');
                } else {
                    errorName = false;
                    $("#error_contact_name").text('');
                }
                if (dataEmail.length < 8) {
                    errorEmail = true;
                    $("#error_contact_email").text('Email Tidak Boleh Kosong');
                } else {
                    errorEmail = false;
                    $("#error_contact_email").text('');
                }
                if (dataWhatsapp.length < 9) {
                    errorWhatsapp = true;
                    $("#error_contact_whatsapp").text('Nomor Handphone Tidak Boleh Kosong');
                } else {
                    errorWhatsapp = false;
                    $("#error_contact_whatsapp").text('');
                }
                if (dataCompany.length < 9) {
                    errorCompany = true;
                    $("#error_contact_company").text('Nama Perusahaan Tidak Boleh Kosong');
                } else {
                    errorCompany = false;
                    $("#error_contact_company").text('');
                }
                console.log(errorCompany + " " + errorWhatsapp + " " + errorE);
                if (errorCompany || errorEmail || errorName || errorWhatsapp) {
                    e.preventDefault();
                }
            })
        })
    </script>
    <script>
        var swiper = new Swiper('.swiperTesti', {
            // Default parameters
            slidesPerView: 1,
            spaceBetween: 15,
            loop: true,
            autoplay: {
                delay: 1500,
            },
            // Responsive breakpoints
            breakpoints: {
                // when window width is >= 320px
                // 450: {
                //     slidesPerView: 2,
                //     spaceBetween: 20
                // },

                // when window width is >= 640px
                769: {
                    slidesPerView: 2,
                    spaceBetween: 25
                }
            }
        })
    </script>
@endsection


<!--end::Entry-->
